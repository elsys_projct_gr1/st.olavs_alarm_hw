Version 4
SymbolType BLOCK
RECTANGLE Normal -96 -24 96 24
WINDOW 0 0 -24 Bottom 2
WINDOW 3 0 24 Top 2
SYMATTR Prefix X
SYMATTR Value GRM31CC80G227ME11
SYMATTR ModelFile C:\Users\eirik\AppData\Local\LTspice\lib\sym\Murrata\High_dielectric_constant\3216_1206\GRM31CC80G227ME11.MOD
PIN -96 0 LEFT 8
PINATTR PinName Port1
PINATTR SpiceOrder 1
PIN 96 0 RIGHT 8
PINATTR PinName Port2
PINATTR SpiceOrder 2
